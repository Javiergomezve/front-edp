export interface Product {
  id: number;
  name: string;
  description: string;
  quality: number;
  size: string;
  picture: string;
  price: number;
  extra_item: string;
  category: string;
  subcategory?: string;
  quantity?: number;
}
