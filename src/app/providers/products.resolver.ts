import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ProductsService } from './products.service';
import { Observable } from 'rxjs';
import { Product } from './product.interface';
@Injectable()
export class ProductsResolver implements Resolve<Observable<Product[]>> {
  constructor(public products: ProductsService) {}

  resolve() {
    return this.products.getProducts();
  }
}
