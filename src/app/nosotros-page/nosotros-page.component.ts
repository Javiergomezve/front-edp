import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var $, jQuery;
@Component({
  selector: 'app-nosotros-page',
  templateUrl: './nosotros-page.component.html',
  styleUrls: ['./nosotros-page.component.css']
})
export class NosotrosPageComponent implements OnInit, AfterViewInit {
  constructor() {}

  ngOnInit() {}
  ngAfterViewInit() {
    $.MultiLanguage('/assets/js/language.json');
  }
}
